from django.db import models
from adulting.account.models import Account
from adulting.category.models import Category

class Transaction(models.Model):
    """
    Transaction activity in the user account. This transaction can be an expense
    or an income.
    """

    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    reference = models.CharField(default="", max_length=50)
    time = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(decimal_places=2, max_digits=20)
    payee = models.CharField(max_length=25)
    notes = models.TextField()

    def save(self, *args, **kwargs):
        super(Transaction, self).save(*args, **kwargs)

    def __str__(self):
        return self.reference
