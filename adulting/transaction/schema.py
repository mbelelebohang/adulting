import graphene
from graphene_django.types import DjangoObjectType
from .models import Transaction

class TransactionType(DjangoObjectType):
    """
    graphene representation of category model.
    """
    class Meta:
        model = Transaction
class TransactionQuery(graphene.ObjectType):
    """
    GraphQL queries for Transection Model.
    """

    all_transaction = graphene.List(TransactionType)

    def resolve_all_transaction(self, info, **kwargs):
        return Transaction.objects.all()
