import graphene
from graphene_django.types import DjangoObjectType
from django.db.models import Q
from .models import Budget

class BudgetType(DjangoObjectType):
    """
    graphene representation of Budget model.
    """
    class Meta:
        model = Budget

class BudgetQuery(graphene.ObjectType):
    """
    GraphQL queries for Budget Model.
    """

    all_budgets = graphene.List(BudgetType, year=graphene.Int())

    def resolve_all_budgets(self, info, year=None, **kwargs):
        if year:
            return Budget.objects.filter(Q(startDate__year=year) | Q(endDate__year=year))

        return Budget.objects.all()
