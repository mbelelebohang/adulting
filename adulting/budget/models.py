from django.db import models

class Budget(models.Model):
    """
    This is the User budget and it holds information about the user
    finances from start date to end date
    """

    startDate = models.DateField()
    endDate = models.DateField()
    amount = models.DecimalField(decimal_places=2, max_digits=20)

    def save(self, *args, **kwargs):
        super(Budget, self).save(*args, **kwargs)

    def __str__(self):
        return self.startDate.strftime('%Y-%m-%d') +' - '+ self.endDate.strftime('%Y-%m-%d')
