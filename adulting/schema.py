import graphene
from adulting.account.schema import AccountQuery
from adulting.budget.schema import BudgetQuery
from adulting.category.schema import CategoryQuery
from adulting.transaction.schema import TransactionQuery

class Query( 
        AccountQuery,
        BudgetQuery,
        CategoryQuery,
        TransactionQuery
    ):
    pass

schema = graphene.Schema(query=Query)
