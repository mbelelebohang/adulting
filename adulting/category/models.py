from django.db import models
from adulting.budget.models import Budget

class CategoryGroup(models.Model):
    """
    This is used to classify categories.
    """

    name = models.CharField(max_length=100)
    budget = models.ForeignKey(Budget, on_delete=models.CASCADE)
    planned = models.DecimalField(decimal_places=2, max_digits=20)

    def save(self, *args, **kwargs):
        super(CategoryGroup, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class Category(models.Model):
    """
    This is used to classify a transection.
    """

    name = models.CharField(max_length=100)
    group = models.ForeignKey(CategoryGroup, on_delete=models.CASCADE)
    isIncome = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
