import graphene
from graphene_django.types import DjangoObjectType
from .models import Category, CategoryGroup

class CategoryType(DjangoObjectType):
    """
    graphene representation of category model.
    """
    class Meta:
        model = Category

class CategoryGroupType(DjangoObjectType):
    """
    graphene representation of category group model.
    """
    class Meta:
        model = CategoryGroup

class CategoryQuery(graphene.ObjectType):
    """
    GraphQL queries for Category Models.
    """

    all_categories = graphene.List(CategoryType)
    all_category_groups = graphene.List(CategoryGroupType)

    def resolve_all_categories(self, info, **kwargs):
        return Category.objects.all()

    def resolve_all_category_groups(self, info, **kwargs):
        return CategoryGroup.objects.all()
