from django.db import models

class Account(models.Model):
    """
    This is the account of the user where the user saves their funds.
    Each account is identified by th unique phone number where the user receives
    notification about the transactions happening on the account.
    """

    name = models.CharField(max_length=100)
    smsNumber = models.CharField(unique=True, max_length=20)
    balance = models.DecimalField(decimal_places=2, max_digits=20)
    active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        super(Account, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
