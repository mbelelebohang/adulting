import graphene
from graphene_django.types import DjangoObjectType
from .models import Account

class AccountType(DjangoObjectType):
    """
    graphene representation of Account model.
    """
    class Meta:
        model = Account

class AccountQuery(graphene.ObjectType):
    """
    GraphQL queries for Account Model.
    """

    all_accounts = graphene.List(AccountType)

    def resolve_all_accounts(self, info, **kwargs):
        return Account.objects.all()
